# SimpleClient: A simple HTTP client on Android issues HTTP requests with RSRP and RSRQ information.
This android program is used to measure HTTP requests benchmarking with RSRP and RSRQ. It is for UMICH EECS 589 course project.

## How to import this project?
This is a standard Android Studio project based on Gradle. Please follow the instruction given by Android Studio on how to import a project.

## Data
- `data/`: The raw data of ApacheBench;
- `data/figures`: The plot program and the result figures.
