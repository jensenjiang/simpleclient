This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 141.212.110.110 (be patient).....done


Server Software:        BaseHTTP/0.3
Server Hostname:        141.212.110.110
Server Port:            8080

Document Path:          /
Document Length:        942995 bytes

Concurrency Level:      1
Time taken for tests:   75.718 seconds
Complete requests:      50
Failed requests:        0
Total transferred:      47155600 bytes
HTML transferred:       47149750 bytes
Requests per second:    0.66 [#/sec] (mean)
Time per request:       1514.366 [ms] (mean)
Time per request:       1514.366 [ms] (mean, across all concurrent requests)
Transfer rate:          608.18 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       53   77  24.7     72     217
Processing:  1083 1437 551.0   1294    4845
Waiting:       38   63  27.5     55     228
Total:       1170 1514 553.3   1377    4929

Percentage of the requests served within a certain time (ms)
  50%   1377
  66%   1472
  75%   1504
  80%   1656
  90%   1921
  95%   2108
  98%   4929
  99%   4929
 100%   4929 (longest request)
