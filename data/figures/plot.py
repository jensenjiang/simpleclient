import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['figure.figsize'] = (8, 4.5)
plt.rcParams.update({'font.size': 14})
plt.rcParams.update({'lines.markersize': 12})

# Figure 1
default_rsrq_th = np.array([[-7, 608.18],
                           [-8, 671.29],
                           [-9, 700.38],
                           [-10, 622.85]])

dynamic_rsrq_th = np.array([[-7, 1647.85],
                            [-8, 1404.81],
                            [-8, 1412.39],
                            [-9, 671.98],
                            [-10, 939.87],
                            [-11, 747.70],
                            [-13, 699.99],
                            [-14, 687.60]])

plt.scatter(default_rsrq_th[:, 0], default_rsrq_th[:, 1], c='red')
plt.scatter(dynamic_rsrq_th[:, 0], dynamic_rsrq_th[:, 1], c='blue', marker='^')
plt.legend(['Default Initial CWND', 'Dynamic Initial CWND'])
plt.xlabel('RSRQ(dB)')
plt.ylabel('Throughput(MByte/sec)')
plt.savefig('fig1_rsrq_throughput', dpi=300)
plt.clf()

# Figure 2
default_rsrp_th = np.array([[-80, 671.29],
                            [-86, 608.18],
                            [-105, 700.38],
                            [-106, 622.85]])

dynamic_rsrp_th = np.array([[-75, 1404.81],
                            [-80, 747.70],
                            [-85, 1647.85],
                            [-90, 1412.39],
                            [-95, 1132.08],
                            [-96, 687.60],
                            [-100, 671.98],
                            [-100, 699.99],
                            [-105, 1375.08]])

plt.scatter(default_rsrp_th[:, 0], default_rsrp_th[:, 1], c='red')
plt.scatter(dynamic_rsrp_th[:, 0], dynamic_rsrp_th[:, 1], c='blue', marker='^')
plt.legend(['Default Initial CWND', 'Dynamic Initial CWND'])
plt.xlabel('RSRP(dBm)')
plt.ylabel('Throughput(MByte/sec)')
plt.savefig('fig2_rsrp_throughput', dpi=300)
plt.clf()

# Figure 3
default_rsrq_time = np.array([[-7, 1514.366],
                              [-8, 1372.000],
                              [-9, 1315.006],
                              [-10, 1478.694]])

dynamic_rsrq_time = np.array([[-6, 524.453],
                              [-7, 558.916],
                              [-8, 652.090],
                              [-8, 655.608],
                              [-9, 1370.598],
                              [-10, 979.927],
                              [-11, 1231.784],
                              [-13, 1315.744],
                              [-14, 1339.455]])

plt.scatter(default_rsrq_time[:, 0], default_rsrq_time[:, 1], c='red')
plt.scatter(dynamic_rsrq_time[:, 0], dynamic_rsrq_time[:, 1], c='blue', marker='^')
plt.legend(['Default Initial CWND', 'Dynamic Initial CWND'])
plt.xlabel('RSRQ(dB)')
plt.ylabel('Time per Request(msec)')
plt.savefig('fig3_rsrq_time', dpi=300)
