package com.example.simpleclient;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.CellInfo;
import android.telephony.CellInfoLte;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private Button cbutton;
    private Button mbutton;
    private Timer timer;
    private TimerTask timerTask;
    private TelephonyManager tm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        cbutton = findViewById(R.id.button_start_stop);
        cbutton.setOnClickListener(new View.OnClickListener() {
            private boolean has_start = false;

            @Override
            public void onClick(View view) {
                Button curr_button = (Button) view;
                if (has_start) {
                    stopTimer();
                    curr_button.setText(R.string.button_start);
                    has_start = false;
                }
                else {
                    if (startTimer()) {
                        curr_button.setText(R.string.button_stop);
                        has_start = true;
                    }
                }
            }
        });

        mbutton = findViewById(R.id.button_measure);
        mbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createMeasureTask();
            }
        });
    }

    public void createMeasureTask() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET},
                    2);
        }
        else if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
        }
        else {
            EditText et1 = findViewById(R.id.addrInput);
            EditText et2 = findViewById(R.id.totRunInput);
            String addr = et1.getText().toString();
            int totRun = Integer.valueOf(et2.getText().toString());
            et1.clearFocus();
            et2.clearFocus();
            new MeasureTask(totRun).run(tm, addr);
        }
    }

    public boolean startTimer() {
        timer = new Timer();
        if (initializeTimerTask()) {
            timer.schedule(timerTask, 0, 1000);
            return true;
        }
        else return false;
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public boolean initializeTimerTask() {
        // Request permission for getAllCellInfo
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
            return false;
        }
        else {
            List<CellInfo> cellInfoList = tm.getAllCellInfo();
            final List<CellInfoLte> lteCellInfoList = new ArrayList<CellInfoLte>();
            for (CellInfo cellInfo : cellInfoList) {
                if (cellInfo instanceof CellInfoLte) {
                    Log.i("LteCell", "Found carrier: " + ((CellInfoLte) cellInfo).getCellIdentity().getMobileNetworkOperator());
                    lteCellInfoList.add((CellInfoLte) cellInfo);
                }
            }
            Log.i("LteCell", "Found LTE cells: " + Integer.toString(lteCellInfoList.size()));
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (!lteCellInfoList.isEmpty()) {
                        Log.i("Rsrp", Integer.toString(lteCellInfoList.get(0).getCellSignalStrength().getRsrp()));
                        Log.i("Rsrq", Integer.toString(lteCellInfoList.get(0).getCellSignalStrength().getRsrq()));
                    }
                }
            };
            return true;
        }
    }
}

class MeasureTask {
    private int totRun = 50;
    private int currRun = 0;
    private long totBytes = 0;
    long startTime;

    public MeasureTask(int _tot) {
        totRun = _tot;
    }

    public void run(TelephonyManager tm, String addr) {
        // Request permission for getAllCellInfo
        List<CellInfo> cellInfoList;
        try {
            cellInfoList = tm.getAllCellInfo();
        }
        catch(SecurityException e) {
            return;
        }
        final List<CellInfoLte> lteCellInfoList = new ArrayList<CellInfoLte>();
        for (CellInfo cellInfo : cellInfoList) {
            if (cellInfo instanceof CellInfoLte) {
                Log.i("LteCell", "Found carrier: " + ((CellInfoLte) cellInfo).getCellIdentity().getMobileNetworkOperator());
                lteCellInfoList.add((CellInfoLte) cellInfo);
            }
        }
        Log.i("LteCell", "Found LTE cells: " + Integer.toString(lteCellInfoList.size()));

        System.out.println("Run with address: " + addr);
        System.out.println("Run " + totRun + " times.");

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(addr)
                //.addHeader("Accept-Encoding", "identity")
                .addHeader("LTEPARAMS", lteCellInfoList.get(0).getCellSignalStrength().getRsrp() + ":"
                                                + lteCellInfoList.get(0).getCellSignalStrength().getRsrq())
                .build();

        startTime = System.currentTimeMillis();
        for (int i = 0;i < totRun;i++) {
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    System.out.println("Failed HTTP Request.");
                    System.out.println(e.getMessage());
                    finalSummary();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    long contentLength = response.body().contentLength();
                    if (contentLength != -1) {
                        totBytes += contentLength;
                    }
                    else {
                        totBytes += response.body().bytes().length;
                    }
                    finalSummary();
                }

                private void finalSummary() {
                    currRun++;
                    if (currRun == totRun) {
                        long timeElapsed = System.currentTimeMillis() - startTime;
                        System.out.println("Total Bytes: " + totBytes);
                        System.out.println("Elapsed Time: " + timeElapsed + "ms");
                        double th = (totBytes / (1024.0 * 1024.0)) / (timeElapsed / 1000.0);
                        System.out.println("Throughput: " + th);
                    }
                }
            });
        }
    }
}
